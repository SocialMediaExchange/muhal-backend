# -*- coding: utf-8 -*-
# Generated by Django 1.11.28 on 2020-02-11 15:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cases', '0005_auto_20200211_1540'),
    ]

    operations = [
        migrations.AddField(
            model_name='case',
            name='current_status',
            field=models.CharField(blank=True, choices=[('open', 'Open'), ('closed', 'Closed'), ('abstenia', 'In abstentia subject to appeal or objection'), ('na', 'Not applicable')], max_length=6, null=True, verbose_name='status'),
        ),
        migrations.AlterField(
            model_name='case',
            name='status',
            field=models.IntegerField(choices=[(1, 'Draft'), (2, 'Published')], default=2, help_text='With Draft chosen, will only be shown for admin users on the site.', verbose_name='Status'),
        ),
    ]
