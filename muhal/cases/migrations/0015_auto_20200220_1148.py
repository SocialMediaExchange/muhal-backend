# -*- coding: utf-8 -*-
# Generated by Django 1.11.28 on 2020-02-20 11:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cases', '0014_auto_20200220_1124'),
    ]

    operations = [
        migrations.AddField(
            model_name='case',
            name='contacted_via',
            field=models.CharField(blank=True, choices=[('phone', 'Phone call'), ('inperson', 'In person'), ('fax', 'Fax'), ('No', 'No')], max_length=10, null=True, verbose_name='Contacted via'),
        ),
        migrations.AddField(
            model_name='case',
            name='content_deletion',
            field=models.CharField(blank=True, choices=[('forced', 'Forced'), ('required', 'Required'), ('No', 'No'), ('n/a', 'Not applicable')], max_length=10, null=True, verbose_name='Requested to delete content?'),
        ),
        migrations.AddField(
            model_name='case',
            name='detained',
            field=models.CharField(blank=True, choices=[('yes', 'Yes'), ('no', 'No'), ('n/a', 'Not applicable')], max_length=6, null=True, verbose_name='detained?'),
        ),
        migrations.AddField(
            model_name='case',
            name='detained_for',
            field=models.IntegerField(blank=True, null=True, verbose_name='detained for number of days'),
        ),
        migrations.AddField(
            model_name='case',
            name='in_absentia',
            field=models.CharField(blank=True, choices=[('yes', 'Yes'), ('no', 'No'), ('n/a', 'Not applicable')], max_length=6, null=True, verbose_name='in absentia?'),
        ),
        migrations.AddField(
            model_name='case',
            name='pledge_signing',
            field=models.CharField(blank=True, choices=[('yes', 'Yes'), ('no', 'No'), ('refuse', 'Refused'), ('n/a', 'Not applicable')], max_length=6, null=True, verbose_name='Requested to sign pledge?'),
        ),
        migrations.AddField(
            model_name='case',
            name='sentence',
            field=models.TextField(blank=True, null=True, verbose_name='sentence'),
        ),
        migrations.AddField(
            model_name='case',
            name='sentenced',
            field=models.CharField(blank=True, choices=[('yes', 'Yes'), ('no', 'No'), ('postponed', 'Postponed'), ('inprocess', 'In the process'), ('n/a', 'Not applicable')], max_length=6, null=True, verbose_name='sentenced?'),
        ),
    ]
