# -*- coding: utf-8 -*-
# Generated by Django 1.11.28 on 2020-02-20 16:56
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cases', '0020_auto_20200220_1654'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='judge',
            name='gender',
        ),
        migrations.AddField(
            model_name='judge',
            name='first_name_ar',
            field=models.CharField(max_length=40, null=True, verbose_name='first name'),
        ),
        migrations.AddField(
            model_name='judge',
            name='first_name_en',
            field=models.CharField(max_length=40, null=True, verbose_name='first name'),
        ),
        migrations.AddField(
            model_name='judge',
            name='last_name_ar',
            field=models.CharField(max_length=40, null=True, verbose_name='last name'),
        ),
        migrations.AddField(
            model_name='judge',
            name='last_name_en',
            field=models.CharField(max_length=40, null=True, verbose_name='last name'),
        ),
    ]
